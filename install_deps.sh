#! /bin/bash

set -eu

current_dir=$(cd `dirname $0`; pwd)

REFRESH_SUBMODULE="ON"
if [ $# -gt 0 ]; then
	REFRESH_SUBMODULE=$1
fi

MODULE_NAME="scripts"
SSH_REPOSITORY="git@github.com:wzx3/compile-scripts.git"
TARGET_BRANCHE="master"

if [ ! -d ${MODULE_NAME} ]; then
    echo -e "\033[32m""<---------------------------Start clone [" ${MODULE_NAME} "]--------------------------->""\033[0m"
    git clone -b ${TARGET_BRANCHE} ${SSH_REPOSITORY} ${MODULE_NAME}
    if [ -d ${MODULE_NAME} ]; then
        cd ${MODULE_NAME}
        git submodule sync --recursive
        git submodule update --init --recursive
    fi
    echo -e "\033[32m""<---------------------------Finish clone [" ${MODULE_NAME} "]--------------------------->""\033[0m"
else
    echo -e "\033[32m""The repository [" ${MODULE_NAME} "] is already exist.""\033[0m"
fi
echo -e "\033[32m""<---------------------------Start Refresh [" ${TARGET_BRANCHE} "]--------------------------->""\033[0m"
if [ -d ${MODULE_NAME} ]; then
    cd ${MODULE_NAME}
    git fetch origin
    git checkout ${TARGET_BRANCHE}
    git pull
    cd ..
fi
echo -e "\033[32m""<---------------------------Finish Refresh [" ${TARGET_BRANCHE} "]--------------------------->""\033[0m"