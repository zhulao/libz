# libz

> Directly use zlib. Source Code: https://github.com/madler/zlib

- 平台支持
  - [x] **android** (NDK: r16b)
    - [x] **armeabi-v7a**
    - [x] **arm64-v8a**
  - [x] **iOS**
    - [x] **armv7** 
    - [x] **arm64** 
    - [x] **x86_64**
  - [x] **macOS**
  - [x] **ubuntu** (16.04.5 LTS)
  - [x] **centos** (7.5.1804)
  - [x] **windows** (10)

- 更新日志
  - **v1.2.11**
    - 编译节点: **cacf7f1**
    - 编译日期: **Jan 16, 2017, at 1:29 AM**

